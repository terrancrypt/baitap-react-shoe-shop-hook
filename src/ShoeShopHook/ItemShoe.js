import React from "react";

export default function ItemShoe(props) {
  let { name, price, image } = props.shoe;
  let renderShoeList = () => {
    return (
      <div className="card">
        <img className="card-img-top" src={image} alt="Card image cap" />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">$ {price}</p>
          <a
            href="#"
            className="btn btn-primary"
            onClick={()=>props.handleOnClick(props.shoe)}
          >
            Add to cart
          </a>
        </div>
      </div>
    );
  };
  return <div className="col-3 p-2">{renderShoeList()}</div>;
}
