import React from "react";
import ItemShoe from "./ItemShoe";

export default function ListShoe(props) {
  let mapItem = () => {
    return props.shoeList.map((item) => {
      return <ItemShoe shoe={item} handleOnClick={props.handleAddToCart}/>;
    });
  };
  return <div className="row">{mapItem()}</div>;
}
