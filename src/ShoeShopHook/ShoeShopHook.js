import React, { useState } from "react";
import Cart from "./Cart";
import { dataShoe } from "./Data_shoe";
import ListShoe from "./ListShoe";

export default function ShoeShopHook() {
  const [shoeArr, setShoeArr] = useState(dataShoe);
  const [cart, setCart] = useState([]);

  let handleAddToCart = (shoe) => {
    let cloneCart = [...cart];

    let index = cloneCart.findIndex((item) => {
      return item.id === shoe.id;
    });

    if (index == -1) {
      let shoeInCart = {
        ...shoe,
        soLuong: 1,
      };
      cloneCart.push(shoeInCart);
    } else {
      cloneCart[index].soLuong++;
    }

    setCart(cloneCart);
  };

  let handleChangeQuantity = (index, hanhDong) => {
    let cloneCartData = [...cart];

    if (hanhDong === "tang") {
      cloneCartData[index].soLuong += 1;
    } else if (hanhDong === "giam") {
      cloneCartData[index].soLuong -= 1;
      if (cloneCartData[index].soLuong === 0) {
        cloneCartData.splice(index, 1);
      }
    }

    setCart(cloneCartData);
  };

  let handleRemoveItem = (id) => {
    let cloneCartData = [...cart];

    cloneCartData.splice(id, 1);

    setCart(cloneCartData);
  };

  return (
    <div className="container">
      <h1>Shoe Shop Hook</h1>
      <Cart
        data={cart}
        handleChangeQuantity={handleChangeQuantity}
        handleRemoveItem={handleRemoveItem}
      />
      <ListShoe shoeList={shoeArr} handleAddToCart={handleAddToCart} />
    </div>
  );
}
