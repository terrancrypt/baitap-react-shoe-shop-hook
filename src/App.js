import logo from './logo.svg';
import './App.css';
import ShoeShopHook from './ShoeShopHook/ShoeShopHook';

function App() {
  return (
    <div className="App">
      <ShoeShopHook/>
    </div>
  );
}

export default App;
